{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    rust-overlay.url = "github:oxalica/rust-overlay";
    systems.url = "github:nix-systems/default-linux";
  };

  outputs = { nixpkgs, rust-overlay, systems, ... }:
    let
      eachSystem = nixpkgs.lib.genAttrs (import systems);

      mkRexoder = { rustPlatform, lib, ... }: rustPlatform.buildRustPackage {
        pname = "redoxer";
        version = "0.2.38";

        src = ./.;
        cargoLock.lockFile = ./Cargo.lock;

        meta = {
          description = "The tool used to build/run Rust programs (and C/C++ programs with zero dependencies) inside of a Redox VM.";
          homepage = "https://gitlab.redox-os.org/redox-os/redoxer";
        };
      };
    in
    {
      overlays.default = final: prev: {
        redoxer = prev.callPackage mkRexoder { };
      };

      devShells = eachSystem (system:
        let
          pkgs = import nixpkgs {
            inherit system;

            overlays = [ rust-overlay.overlays.default ];
          };

          rust-toolchain = (pkgs.rust-bin.fromRustupToolchainFile ./rust-toolchain.toml).override {
            extensions = [ "rust-analyzer" ];
          };
        in
        {
          default = pkgs.mkShell {
            packages = with pkgs; [
              fuse3
            ] ++ [ rust-toolchain ];
          };
        });
    };
}
